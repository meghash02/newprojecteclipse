package com.pack;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com")
public class SpringBootApp 
{
	private static final Logger logger = LoggerFactory.getLogger(SpringBootApp.class);
     public static void main(String[] args) 
     {
         SpringApplication.run(SpringBootApp.class, args);
         

        
     }
     

      
}