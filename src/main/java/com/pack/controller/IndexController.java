package com.pack.controller;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.pack.SpringBootApp;

@Controller
public class IndexController {

	private static final Logger logger = LoggerFactory.getLogger(SpringBootApp.class);
	@RequestMapping("/")
	public String home(Map<String, Object> model)
	{
		model.put("message","HowToDoInJava Reader Welcome !!");
		return "index";
	}
	@RequestMapping("/next")
	public String next(Map<String, Object> model)
	{
		model.put("message","You are in new page !!");
		logger.warn("hello world");
		return "next";
	}
}
